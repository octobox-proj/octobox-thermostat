EESchema Schematic File Version 4
LIBS:octobox-thermostat-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "OctoBox AUX board"
Date "2018-09-17"
Rev "1"
Comp "Avishay Orpaz"
Comment1 "avishorp@gmail.com"
Comment2 "https://creativecommons.org/licenses/by-sa/4.0/"
Comment3 "License: Attribution Share-Alike 4.0"
Comment4 "Released under Creative Commons"
$EndDescr
Wire Wire Line
	5700 6400 5900 6400
Wire Wire Line
	5700 6400 5700 6500
Wire Wire Line
	5700 6500 5900 6500
Connection ~ 5700 6400
Wire Wire Line
	5900 6700 5800 6700
Wire Wire Line
	5800 6700 5800 7200
Wire Wire Line
	5800 7200 5550 7200
Text Label 7200 6550 0    50   ~ 0
DROP
$Comp
L Device:R R1
U 1 1 5BC8831A
P 5550 6900
F 0 "R1" H 5750 6850 50  0000 C CNN
F 1 "34.2K" H 5750 6950 50  0000 C CNN
F 2 "R_0603_1608Metric" V 5480 6900 50  0001 C CNN
F 3 "~" H 5550 6900 50  0001 C CNN
	1    5550 6900
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 6750 5550 6600
Wire Wire Line
	5550 6600 5900 6600
Wire Wire Line
	5550 7050 5550 7200
Text Notes 6050 7000 0    50   ~ 0
Trip Temp. approx. 60C\nSee datasheet for R1 value
$Comp
L Device:R R2
U 1 1 5BDBB41A
P 7000 6250
F 0 "R2" H 6850 6200 50  0000 C CNN
F 1 "1M" H 6850 6300 50  0000 C CNN
F 2 "R_0603_1608Metric" V 6930 6250 50  0001 C CNN
F 3 "~" H 7000 6250 50  0001 C CNN
	1    7000 6250
	-1   0    0    1   
$EndComp
$Comp
L Device:C C1
U 1 1 5B9E1A88
P 5100 6550
F 0 "C1" H 5215 6596 50  0000 L CNN
F 1 "1uF" H 5215 6505 50  0000 L CNN
F 2 "C_0603_1608Metric" H 5138 6400 50  0001 C CNN
F 3 "~" H 5100 6550 50  0001 C CNN
	1    5100 6550
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5BA88F91
P 14500 9300
F 0 "#LOGO1" H 14500 9575 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 14500 9075 50  0001 C CNN
F 2 "" H 14500 9300 50  0001 C CNN
F 3 "~" H 14500 9300 50  0001 C CNN
	1    14500 9300
	1    0    0    -1  
$EndComp
Text Notes 13900 9650 0    100  ~ 20
CC BY-SA 4.0
$Comp
L Misc:MCP9509 U1
U 1 1 5C3A7E4A
P 6250 6550
F 0 "U1" H 6350 6977 55  0000 C CNN
F 1 "MCP9509" H 6350 6878 55  0000 C CNN
F 2 "SOT-23-5" H 6250 6550 55  0001 C CNN
F 3 "" H 6250 6550 55  0001 C CNN
	1    6250 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 7200 5550 7350
Connection ~ 5550 7200
$Comp
L power:GND #PWR0101
U 1 1 5C426443
P 5550 7350
F 0 "#PWR0101" H 5550 7100 50  0001 C CNN
F 1 "GND" H 5555 7177 50  0000 C CNN
F 2 "" H 5550 7350 50  0001 C CNN
F 3 "" H 5550 7350 50  0001 C CNN
	1    5550 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 6400 5700 6400
Wire Wire Line
	5100 6700 5100 7200
Wire Wire Line
	5100 7200 5550 7200
Wire Wire Line
	6800 6550 7000 6550
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5C426805
P 8150 6550
F 0 "J1" H 8229 6546 50  0000 L CNN
F 1 "Conn_01x03" H 8230 6501 50  0001 L CNN
F 2 "PinHeader_1x03_P2.54mm_Vertical" H 8150 6550 50  0001 C CNN
F 3 "~" H 8150 6550 50  0001 C CNN
	1    8150 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 6400 7000 6550
Connection ~ 7000 6550
Wire Wire Line
	7000 6100 7000 5950
Wire Wire Line
	7000 5950 5700 5950
Wire Wire Line
	5700 5950 5700 6400
Wire Wire Line
	7950 6450 7950 5950
Wire Wire Line
	7950 5950 7550 5950
Connection ~ 7000 5950
Wire Wire Line
	7950 6650 7950 7200
Connection ~ 5800 7200
$Comp
L Device:C C2
U 1 1 5C4275AC
P 7550 6250
F 0 "C2" H 7665 6296 50  0000 L CNN
F 1 "10nF" H 7665 6205 50  0000 L CNN
F 2 "C_0603_1608Metric" H 7588 6100 50  0001 C CNN
F 3 "~" H 7550 6250 50  0001 C CNN
	1    7550 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 6550 7550 6550
Wire Wire Line
	5800 7200 7950 7200
Wire Wire Line
	7550 6100 7550 5950
Connection ~ 7550 5950
Wire Wire Line
	7550 5950 7000 5950
Wire Wire Line
	7550 6400 7550 6550
Connection ~ 7550 6550
Wire Wire Line
	7550 6550 7950 6550
$EndSCHEMATC
